package com.yhy.job.service;

import com.yhy.common.service.BaseMainService;
import com.yhy.job.dao.QuartzJobDao;
import com.yhy.job.vo.QuartzJobVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-16 下午2:43 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class QuartzJobService extends BaseMainService<QuartzJobVO> {


    @Autowired
    private QuartzJobDao baseDao;

    @Override
    protected QuartzJobDao getDao() {
        return baseDao;
    }


    public void updateRunStatus(QuartzJobVO jobVO) {
        getDao().updateRunStatus(jobVO);
    }

}
