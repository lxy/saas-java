package com.yhy.common.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Sets;
import com.yhy.common.constants.BusPrivCode;
import com.yhy.common.dto.BaseEntity;
import com.yhy.common.dto.BaseMainEntity;
import org.apache.commons.lang3.StringUtils;

import java.util.Set;

public class SysUser extends BaseMainEntity {

    private String userAccount;
    private String password;
    private String userName;
    private String avatar;//头像

    private String lanCode;

    private String phone;
    private String email;
    private String cpyCode;
    private String cpyName;
    private String orgCode;
    private String orgName;

    private String weixin;
    private String userStatus;
    private String thirdAccount;

    private String sourceType;

    private Set<String> privOrgCodes;

    private Set<String> roles = Sets.newHashSet();
    private String mgrAccount;//上级编号
    // 用户所属组织
    private Set<String> userOrgCodes;

    public SysUser() {

    }

    public Set<String> getUserOrgCodes() {
        return userOrgCodes;
    }

    public void setUserOrgCodes(Set<String> userOrgCodes) {
        this.userOrgCodes = userOrgCodes;
    }

    public String getMgrAccount() {
        return mgrAccount;
    }

    public void setMgrAccount(String mgrAccount) {
        this.mgrAccount = mgrAccount;
    }

    public Set<String> getPrivOrgCodes() {
        return privOrgCodes;
    }

    public void setPrivOrgCodes(Set<String> privOrgCodes) {
        this.privOrgCodes = privOrgCodes;
    }

    public String getCpyName() {
        return cpyName;
    }

    public void setCpyName(String cpyName) {
        this.cpyName = cpyName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public boolean isSysAdmin() {
        return BusPrivCode.PRIV_SYS_ADMIN.getVal().equalsIgnoreCase(userAccount);
    }

    public SysUser(String userAccount, String password) {
        this.userAccount = userAccount;
        this.password = password;
    }

    public String getThirdAccount() {
        return thirdAccount;
    }

    public void setThirdAccount(String thirdAccount) {
        this.thirdAccount = thirdAccount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpyCode() {
        return cpyCode;
    }

    public void setCpyCode(String cpyCode) {
        this.cpyCode = cpyCode;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getLanCode() {
        return lanCode;
    }

    public void setLanCode(String lanCode) {
        this.lanCode = lanCode;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    @JSONField(serialize=false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
