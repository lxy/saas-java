package com.yhy.common.constants;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-21 下午3:04 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public enum BusClassType {

	/*
	工作流定义管理, Module, 是否启用扩展功能属性定义
	 */
	BCLS_WORKFLOW_DEF_BILL("100010-001",BusModuleType.MD_WORKFLOW_DEF_MNG.getVal(), false, "", "",""),

	BCLS_WORKFLOW_CONFIG_BILL("100011-001",BusModuleType.MD_WORKFLOW_CONFIG_MNG.getVal(), true, "", "",""),

	BCLS_DYNAMIC_FORM_BILL("100012-001",BusModuleType.MD_DYNAMIC_FORM_MNG.getVal(),false, "", "",""),

	BCLS_FORM_APPLY_BILL("100015-001",BusModuleType.MD_FORM_APPLY_MNG.getVal(),false, "formApplyMngService", "apiFormService","/system/form/formApply/module/FormApproveDisplay");

	private final String val;
	private final String busModuleVal;
	private final Boolean extendMng;
	private final String mngService;
	private final String apiService;
	private final String apvCompUrl;

	private static Map<String, BusClassType> classTypeMap;

	BusClassType(String val, String busModuleVal, Boolean extendMng, String mngService, String apiService, String apvCompUrl) {
		this.val = val;
		this.busModuleVal = busModuleVal;
		this.extendMng = extendMng;
		this.mngService = mngService;
		this.apiService = apiService;
		this.apvCompUrl = apvCompUrl;
	}

	public String getVal() {
		return val;
	}

	public String getBusModuleVal() {
		return busModuleVal;
	}


	public Boolean getExtendMng() {
		return extendMng;
	}

	public String getMngService() {
		return mngService;
	}

	public String getApiService() {
		return apiService;
	}

	public String getApvCompUrl() {
		return apvCompUrl;
	}
	/**
	 * 根据操作类型的值获取业务类型
	 * @param val
	 * @return
	 */
	public static BusClassType getInstByVal(String val) {
		if(classTypeMap == null) {
			synchronized (BusClassType.class) {
				if (classTypeMap == null) {
					classTypeMap = new HashMap<String, BusClassType>();
					for (BusClassType moduleType : BusClassType.values()) {
						classTypeMap.put(moduleType.getVal(), moduleType);
					}
				}
			}
		}
		return classTypeMap.get(val);
	}

}
