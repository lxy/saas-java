package com.yhy.common.constants;

import com.yhy.common.exception.BusinessException;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
public enum BusOtherState {

	// 待反馈
	WAIT_FEEDBACK("8","bus.state.wait_feedback"),

	// 已拒绝
	REJECT("4","bus.state.reject"),

	// 已生效
	VALID("10", "bus.state.valid");

	private final String val;
	private final String labCode;

	private static Map<String, BusOtherState> busStateMap;

	BusOtherState(String val, String labCode) {
		this.val = val;
		this.labCode = labCode;
	}

	public String getVal() {
		return val;
	}

	public String getLabel() {
		return labCode;
	}

	public static BusOtherState getInstByVal(String key) {
		if (busStateMap == null) {
			synchronized (BusOtherState.class) {
				if (busStateMap == null) {
					busStateMap = new HashMap<String, BusOtherState>();
					for (BusOtherState busState : BusOtherState.values()) {
						busStateMap.put(busState.getVal() + "", busState);
					}
				}
			}
		}
		if (!busStateMap.containsKey(key)) {
			throw new BusinessException("状态值" + key + "对应的BusOtherState枚举值不存在。");
		}
		return busStateMap.get(key);
	}

}
