package com.yhy.common.dto;

import com.yhy.common.constants.BusSelfState;
import com.yhy.common.constants.FunProcess;
import com.yhy.common.utils.I18nUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.Set;

public class BaseMngVO extends BaseEntity {

    private String busId;
    private String mainId;
    private String selfState;
    private String otherState;
    private String busType;
    private Date busDate;
    private String busDirection;
    private String busModule;
    private String busClass;
    private String funProcess;
    private String relNum;
    private String encryptCode;
    private String forwardType;
    private String otherSysOrgCode;
    private String otherSysOwnerCpy;
    private String historyFlag;
    private Integer busVersion;
    private String parentId;//对应上一版本的Busid
    private String oldMainId;

    private String customCode;
    private String sysGenCode;
    private String busRmk;
    private Boolean isPassDirect = false;
    //设定查询的功能列表
    private Set<String> selfStateList;

    public BaseMngVO() {
    }

    public Boolean getPassDirect() {
        return isPassDirect;
    }

    public void setPassDirect(Boolean passDirect) {
        isPassDirect = passDirect;
    }

    public String getBusRmk() {
        return busRmk;
    }

    public void setBusRmk(String busRmk) {
        this.busRmk = busRmk;
    }

    public Set<String> getSelfStateList() {
        return selfStateList;
    }

    public void setSelfStateList(Set<String> selfStateList) {
        this.selfStateList = selfStateList;
    }

    public String getOldMainId() {
        return oldMainId;
    }

    public void setOldMainId(String oldMainId) {
        this.oldMainId = oldMainId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getFunProcessName() {
        if(StringUtils.isNotBlank(this.funProcess)) {
            FunProcess busFunProcess = FunProcess.getInstByVal(this.funProcess);
            return I18nUtils.get(busFunProcess.getLabel()) + "/" + this.getBusVersion()
                    + "/" + this.getSysGenCode();
        }
        return this.funProcess;
    }

    public String getSelfStateName() {
        if(StringUtils.isNotBlank(this.selfState)) {
            BusSelfState busSelfState = BusSelfState.getInstByVal(this.selfState);
            return  I18nUtils.get(busSelfState.getLabel());
        }
        return this.selfState;
    }

    public String getOtherStateName() {
        if(StringUtils.isNotBlank(this.otherState)) {
            return I18nUtils.get(BusSelfState.getInstByVal(this.otherState).getLabel());
        }
        return this.otherState;
    }

    public Integer getBusVersion() {
        return busVersion;
    }

    public void setBusVersion(Integer busVersion) {
        this.busVersion = busVersion;
    }

    public String getBusClass() {
        return busClass;
    }

    public void setBusClass(String busClass) {
        this.busClass = busClass;
    }

    public String getHistoryFlag() {
        return historyFlag;
    }

    public void setHistoryFlag(String historyFlag) {
        this.historyFlag = historyFlag;
    }

    public String getBusId() {
        return busId;
    }

    public void setBusId(String busId) {
        this.busId = busId;
    }

    public String getMainId() {
        return mainId;
    }

    public void setMainId(String mainId) {
        this.mainId = mainId;
    }

    public String getCustomCode() {
        return customCode;
    }

    public void setCustomCode(String customCode) {
        this.customCode = customCode;
    }

    public String getSysGenCode() {
        return sysGenCode;
    }

    public void setSysGenCode(String sysGenCode) {
        this.sysGenCode = sysGenCode;
    }

    public String getSelfState() {
        return selfState;
    }

    public void setSelfState(String selfState) {
        this.selfState = selfState;
    }

    public String getOtherState() {
        return otherState;
    }

    public void setOtherState(String otherState) {
        this.otherState = otherState;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public Date getBusDate() {
        return busDate;
    }

    public void setBusDate(Date busDate) {
        this.busDate = busDate;
    }

    public String getBusDirection() {
        return busDirection;
    }

    public void setBusDirection(String busDirection) {
        this.busDirection = busDirection;
    }

    public String getBusModule() {
        return busModule;
    }

    public void setBusModule(String busModule) {
        this.busModule = busModule;
    }

    public String getFunProcess() {
        return funProcess;
    }

    public void setFunProcess(String funProcess) {
        this.funProcess = funProcess;
    }

    public String getRelNum() {
        return relNum;
    }

    public void setRelNum(String relNum) {
        this.relNum = relNum;
    }

    public String getEncryptCode() {
        return encryptCode;
    }

    public void setEncryptCode(String encryptCode) {
        this.encryptCode = encryptCode;
    }

    public String getForwardType() {
        return forwardType;
    }

    public void setForwardType(String forwardType) {
        this.forwardType = forwardType;
    }

    public String getOtherSysOrgCode() {
        return otherSysOrgCode;
    }

    public void setOtherSysOrgCode(String otherSysOrgCode) {
        this.otherSysOrgCode = otherSysOrgCode;
    }

    public String getOtherSysOwnerCpy() {
        return otherSysOwnerCpy;
    }

    public void setOtherSysOwnerCpy(String otherSysOwnerCpy) {
        this.otherSysOwnerCpy = otherSysOwnerCpy;
    }
}
