package com.yhy.common.config;

import com.yhy.common.utils.ConstantUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午11:12 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Component
@Order(value = 1)
public class SystemConfigStart implements CommandLineRunner {

    protected Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public void run(String... strings) throws Exception {
        ConstantUtil.DB_TYPE = EnvConfig.getValueByPropertyName("pagehelper.helperDialect");
    }


}
