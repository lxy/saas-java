package com.yhy.common.service;

import com.yhy.common.dao.BaseDao;
import com.yhy.common.dao.BaseMainDao;
import com.yhy.common.dao.BaseMngDao;
import com.yhy.common.dto.BaseDTO;
import com.yhy.common.dto.BaseEntity;
import com.yhy.common.dto.BaseMainEntity;
import com.yhy.common.dto.BaseMngVO;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-26 下午3:11 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public abstract class BaseMainService<E extends BaseMainEntity> extends BaseService<E> {

    protected Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    protected BaseMainDao<E> getDao() {
        return null;
    }

    @Override
    protected void preInsert(E entity) {
        if(StringUtils.isBlank(entity.getId())) {
            entity.setId(YhyUtils.genId(entity.getBusModule()));
        }
        super.preInsert(entity);
    }

    /*
    * 用于简单业务保留
     */
    public int updateEnableFlagForRecover(String id, String enableFlag) {
        return getDao().updateEnableFlagForRecover(id, enableFlag);
    }

    public int updateVersion(E entity) {
        preUpdate(entity);
        return getDao().updateVersion(entity);
    }

    public int updateBusState(E entity) {
        preUpdate(entity);
        return getDao().updateBusState(entity);
    }

}
