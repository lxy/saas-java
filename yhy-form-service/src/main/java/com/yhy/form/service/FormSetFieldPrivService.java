package com.yhy.form.service;

import com.google.common.collect.Lists;
import com.yhy.admin.api.IAdminService;
import com.yhy.common.service.BaseService;
import com.yhy.form.dao.FormSetFieldPrivDao;
import com.yhy.form.dto.FormFieldPrivDTO;
import com.yhy.form.vo.FormSetFieldPrivVO;
import com.yhy.form.vo.FormSetFieldVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午11:48 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class FormSetFieldPrivService extends BaseService<FormSetFieldPrivVO> {


    @Autowired
    private FormSetFieldPrivDao baseDao;
    @Autowired
    private FormSetFieldService formSetFieldService;

    @Autowired
    @Qualifier(IAdminService.SERVICE_BEAN)
    private IAdminService adminService;

    @Override
    protected FormSetFieldPrivDao getDao() {
        return baseDao;
    }

    public List<FormSetFieldPrivVO> findByMainId(String mainId) {
        return getDao().findByMainId(mainId);
    }

    public List<FormSetFieldPrivVO> findByFieldId(String fieldId) {
        return getDao().findByFieldId(fieldId);
    }

    public void setFormDataPriv(String formId, String userAccount, Map<String,Boolean> formDataEditable, Map<String,Boolean> formDataVisible) {
        List<FormSetFieldPrivVO> formSetFieldPrivVOList = this.findByMainId(formId);

        for (FormSetFieldPrivVO detailVO : formSetFieldPrivVOList) {
            boolean hasPriv = this.fieldHasPriv(userAccount, detailVO.getTypeCode(), detailVO.getTypeValue());
            switch (detailVO.getPrivType()) {
                case FormSetFieldPrivVO.VISIBLE_PRIV_TYPE:
                    formDataVisible.put(detailVO.getFieldControlId(), hasPriv);
                    break;
                case FormSetFieldPrivVO.EDIT_PRIV_TYPE:
                    formDataEditable.put(detailVO.getFieldControlId(), hasPriv);
                    break;
            }
        }
    }

    /*
    * 根据用户 及设定的值判断否有权限
     */
    private Boolean fieldHasPriv(String userAccount, String typeCode, String typeValue) {
        if("USER".equals(typeCode)) {
            if(typeValue.equals(userAccount)) {
                return true;
            }
        } else if("ROLE".equals(typeCode)) {
            return adminService.getUserByRoleId(typeValue).stream().filter(vo -> vo.getUserAccount().equals(userAccount)).count() > 0 ? true : false;
        }
        return false;
    }

    private String getNameByTypeValue(String typeCode, String typeValue, String cpyCode) {
        if("USER".equals(typeCode)) {
            return adminService.getUserByUserAccountAndCpyCode(typeValue, cpyCode).getUserName();
        } else if("ROLE".equals(typeCode)) {
            return adminService.getRoleByRoleId(typeValue).getRoleName();
        }
        return "undefine";
    }

    public FormFieldPrivDTO convertDetail2Info(String fieldId, List<FormSetFieldPrivVO> configDetailVOS) {
        //设定默认值
        FormFieldPrivDTO configInfo = new FormFieldPrivDTO();
        configInfo.setFieldVO(formSetFieldService.findById(fieldId));

        FormSetFieldPrivVO visibleFieldPrivVO = new FormSetFieldPrivVO();
        configInfo.setVisibleFieldPrivVO(visibleFieldPrivVO);
        visibleFieldPrivVO.setPrivType(FormSetFieldPrivVO.VISIBLE_PRIV_TYPE);
        visibleFieldPrivVO.setTypeCode("USER");

        FormSetFieldPrivVO editFieldPrivVO = new FormSetFieldPrivVO();
        configInfo.setEditFieldPrivVO(editFieldPrivVO);
        editFieldPrivVO.setPrivType(FormSetFieldPrivVO.EDIT_PRIV_TYPE);
        editFieldPrivVO.setTypeCode("USER");

        StringBuffer visibleTypeValue = new StringBuffer();
        StringBuffer visibleTypeName = new StringBuffer();

        StringBuffer editTypeValue = new StringBuffer();
        StringBuffer editTypeName = new StringBuffer();

        for (FormSetFieldPrivVO detailVO : configDetailVOS) {
            switch (detailVO.getPrivType()) {
                case FormSetFieldPrivVO.VISIBLE_PRIV_TYPE:
                    visibleFieldPrivVO.setTypeCode(detailVO.getTypeCode());
                    visibleTypeValue = visibleTypeValue.append(detailVO.getTypeValue()).append(",");
                    visibleTypeName =  visibleTypeName.append(getNameByTypeValue(detailVO.getTypeCode(),detailVO.getTypeValue(),detailVO.getSysOwnerCpy())).append(",");
                    break;
                case FormSetFieldPrivVO.EDIT_PRIV_TYPE:
                    editFieldPrivVO.setTypeCode(detailVO.getTypeCode());
                    editTypeValue = editTypeValue.append(detailVO.getTypeValue()).append(",");
                    editTypeName =  editTypeName.append(getNameByTypeValue(detailVO.getTypeCode(),detailVO.getTypeValue(),detailVO.getSysOwnerCpy())).append(",");
                    break;
            }
        }
        if(StringUtils.isNotBlank(visibleTypeValue.toString())) {
            visibleFieldPrivVO.setTypeValue(visibleTypeValue.toString());
            visibleFieldPrivVO.setTypeName(visibleTypeName.toString());
        }
        if(StringUtils.isNotBlank(editTypeValue.toString())) {
            editFieldPrivVO.setTypeValue(editTypeValue.toString());
            editFieldPrivVO.setTypeName(editTypeName.toString());
        }

        return configInfo;
    }

    public List<FormSetFieldPrivVO> convertInfo2DetailVO(FormSetFieldVO fieldVO,
                                                         FormSetFieldPrivVO visibleFieldPrivVO, FormSetFieldPrivVO editFieldPrivVO) {
        List<FormSetFieldPrivVO> newDetails = Lists.newArrayList();

        if(visibleFieldPrivVO != null) {
            //可见权限控制
            if(StringUtils.isNotBlank(visibleFieldPrivVO.getTypeValue())) {
                for (String typeValue : visibleFieldPrivVO.getTypeValue().split(",")) {
                    FormSetFieldPrivVO privVO = new FormSetFieldPrivVO();
                    privVO.setMainId(fieldVO.getMainId());
                    privVO.setFieldId(fieldVO.getId());
                    privVO.setFieldControlId(fieldVO.getFieldControlId());
                    privVO.setPrivType(FormSetFieldPrivVO.VISIBLE_PRIV_TYPE);
                    privVO.setTypeCode(visibleFieldPrivVO.getTypeCode());
                    privVO.setTypeValue(typeValue);
                    newDetails.add(privVO);
                }
            }
        }

        if(editFieldPrivVO != null) {
            //编辑权限控件
            if(StringUtils.isNotBlank(editFieldPrivVO.getTypeValue())) {
                for (String typeValue : editFieldPrivVO.getTypeValue().split(",")) {
                    FormSetFieldPrivVO privVO = new FormSetFieldPrivVO();
                    privVO.setMainId(fieldVO.getMainId());
                    privVO.setFieldId(fieldVO.getId());
                    privVO.setFieldControlId(fieldVO.getFieldControlId());
                    privVO.setPrivType(FormSetFieldPrivVO.EDIT_PRIV_TYPE);
                    privVO.setTypeCode(editFieldPrivVO.getTypeCode());
                    privVO.setTypeValue(typeValue);
                    newDetails.add(privVO);
                }
            }
        }

        return newDetails;
    }

}
