package com.yhy.form.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yhy.form.dao.FormSetFieldListDao;
import com.yhy.form.vo.FormSetFieldListVO;
import com.yhy.common.service.BaseService;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-17 下午3:36 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-15 下午2:25 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class FormSetFieldListService extends BaseService<FormSetFieldListVO> {

	@Autowired
	private  FormSetFieldListDao formSetFieldListDao;

	@Override
	protected FormSetFieldListDao getDao() {
		return formSetFieldListDao;
	}

	public List<FormSetFieldListVO> findByMainId(String mainId, String fieldControlId) {
		return getDao().findByMainId(mainId, fieldControlId);
	}

	public Integer deleteByMainId(String mainId, String fieldControlId) {
		return getDao().deleteByMainId(mainId, fieldControlId);
	}

}
