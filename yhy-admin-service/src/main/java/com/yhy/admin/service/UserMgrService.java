package com.yhy.admin.service;

import com.yhy.admin.dao.UserMgrDao;
import com.yhy.admin.vo.UserMgrVO;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-12-26 下午5:39 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserMgrService extends BaseService<UserMgrVO> {

    @Autowired
    private UserMgrDao userMgrDao;

    @Override
    protected UserMgrDao getDao() {
        return userMgrDao;
    }

    @Override
    protected void preInsert(UserMgrVO entity) {
        super.preInsert(entity);
    }

    public int deleteByUserAccount(String cpyCode, String userAccount) {
        return getDao().deleteByUserAccount(cpyCode, userAccount);
    }

    public UserMgrVO findMgrByUserAccount(String cpyCode, String userAccount) {
        return getDao().findMgrByUserAccount(cpyCode, userAccount);
    }

    public void saveUserMgr(UserMgrVO userMgrVO) {
        if(userMgrVO.getUserAccount().equals(userMgrVO.getMgrAccount())) {
            throw new BusinessException("上级与当前账号不能一样");
        }
        this.deleteByUserAccount(userMgrVO.getCpyCode(),userMgrVO.getUserAccount());
        if(StringUtils.isNotBlank(userMgrVO.getMgrAccount())) {
            userMgrVO.setEnableFlag("Y");
            insert(userMgrVO);
        }
    }


}
