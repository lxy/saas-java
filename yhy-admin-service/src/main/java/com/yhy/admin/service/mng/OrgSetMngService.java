package com.yhy.admin.service.mng;

import com.yhy.admin.dao.OrgSetMngDao;
import com.yhy.admin.dto.OrgSetDTO;
import com.yhy.admin.service.OrgSetService;
import com.yhy.admin.service.UserOrgSetService;
import com.yhy.admin.vo.OrgSetVO;
import com.yhy.admin.vo.UserOrgSetVO;
import com.yhy.admin.vo.mng.OrgSetMngVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseMngService;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-13 下午4:25 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OrgSetMngService extends BaseMngService<OrgSetDTO,OrgSetMngVO> {

    @Autowired
    private OrgSetMngDao orgSetMngDao;

    @Autowired
    private OrgSetService orgSetService;
    @Autowired
    private SysUserMngService sysUserMngService;

    @Autowired
    private UserOrgSetService userOrgSetService;


    @Override
    protected OrgSetMngDao getBaseMngDao() {
        return orgSetMngDao;
    }

    @Override
    public String getBusModule() {
        return BusModuleType.MD_ORG_SET_MNG.getVal();
    }

    @Override
    public OrgSetService getBaseMainService() {
        return orgSetService;
    }

    @Override
    protected void beforeSaveOfProcess(OrgSetDTO baseDTO) {
        super.beforeSaveOfProcess(baseDTO);
        OrgSetMngVO busMainData = baseDTO.getBusMainData();
        OrgSetVO paramVO = new OrgSetVO();
        paramVO.setEnableFlag("Y");
        paramVO.setThirdCode(busMainData.getThirdCode());
        paramVO.setCpyCode(busMainData.getCpyCode());
        paramVO.setOrgType(busMainData.getOrgType());
        List<OrgSetVO> existsList = getBaseMainService().findBy(paramVO);
        for (OrgSetVO tmpVO : existsList) {
            if(!tmpVO.getId().equals(busMainData.getId())) {
                throw new BusinessException("相同的编号已存在");
            }
        }
    }

    public String autoCreateDefaultOrgSet(String cpyCode) {
        OrgSetDTO orgSetDTO = new OrgSetDTO();
        OrgSetMngVO mngVO = new OrgSetMngVO();
        mngVO.setCpyCode(cpyCode);
        mngVO.setSysOwnerCpy(cpyCode);
        mngVO.setOrgName("默认机构");
        mngVO.setOrgType("1");
        mngVO.setOrgStatus("1");
        mngVO.setThirdCode("-1");
        mngVO.setEnableFlag("Y");
        orgSetDTO.setBusMainData(mngVO);

        doSave(orgSetDTO);

        return mngVO.getId();
    }

    @Override
    protected void processOfToAdd(OrgSetDTO baseDTO) {
        super.processOfToAdd(baseDTO);
        OrgSetMngVO mngVO = baseDTO.getBusMainData();
        mngVO.setOrgType("1");
        mngVO.setOrgStatus("1");
        mngVO.setOrgCode(null);
        mngVO.setEnableFlag("Y");
    }

    @Override
    protected void processOfToCopy(OrgSetDTO baseDTO) {
        super.processOfToCopy(baseDTO);
        OrgSetMngVO mngVO = baseDTO.getBusMainData();
        mngVO.setOrgCode(null);
        mngVO.setEnableFlag("Y");
    }

    @Override
    protected void beforeDeleteOfProcess(OrgSetMngVO mngVO) {
        UserOrgSetVO paramBean = new UserOrgSetVO();
        paramBean.setOrgCode(mngVO.getOrgCode());
        paramBean.setEnableFlag("Y");
        if (userOrgSetService.hasBy(paramBean)) {
            throw new BusinessException(mngVO.getOrgName()+"该组织下有用户不能删除.");
        }
    }

    public Set<OrgSetMngVO> buildOrgTree(List<OrgSetMngVO> orgSetMngVOS) {
        Set<OrgSetMngVO> trees = new LinkedHashSet<>();
        for (OrgSetMngVO orgSetMngVO : orgSetMngVOS) {
            if (StringUtils.isBlank(orgSetMngVO.getParentId())) {
                trees.add(orgSetMngVO);
            }
            for (OrgSetMngVO it : orgSetMngVOS) {
                if (orgSetMngVO.getId().equals(it.getParentId())) {
                    if (orgSetMngVO.getChildren() == null) {
                        orgSetMngVO.setChildren(new ArrayList<OrgSetMngVO>());
                    }
                    orgSetMngVO.getChildren().add(it);
                }
            }
        }
        return trees;
    }

    public AppReturnMsg saveUserOrgSet(OrgSetDTO baseDTO) {
        if(baseDTO.getUserOrgSetVOList() == null || baseDTO.getUserOrgSetVOList().isEmpty()) {
            throw new BusinessException("用户组织关系不能为空");
        }
        for (UserOrgSetVO userOrgSetVO : baseDTO.getUserOrgSetVOList()) {
            if(!userOrgSetService.hasBy(userOrgSetVO)) {
                userOrgSetVO.setEnableFlag("Y");
                userOrgSetService.saveOrUpdate(userOrgSetVO);

                SysUser tmpUser = sysUserMngService.findByUserCpyCode(userOrgSetVO.getUserAccount(), userOrgSetVO.getCpyCode());
                // 默认设置部门
                if(StringUtils.isBlank(tmpUser.getOrgCode()) && userOrgSetVO.getCpyCode().equals(tmpUser.getCpyCode())) {
                    sysUserMngService.updateOrgSet(tmpUser.getUserAccount(), userOrgSetVO.getOrgCode());
                }
            }
        }
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"保存成功",baseDTO,null);
    }

    public AppReturnMsg deleteUserOrgSet(OrgSetDTO baseDTO) {
        if(baseDTO.getUserOrgSetVOList() == null || baseDTO.getUserOrgSetVOList().isEmpty()) {
            throw new BusinessException("用户组织关系不能为空");
        }
        for (UserOrgSetVO userOrgSetVO : baseDTO.getUserOrgSetVOList()) {
            UserOrgSetVO tmpvo = userOrgSetService.findById(userOrgSetVO.getId());
            userOrgSetService.deleteById(userOrgSetVO);

            SysUser tmpUser = sysUserMngService.findByUserCpyCode(tmpvo.getUserAccount(),tmpvo.getCpyCode());
            // 删除的部门与当前用户所属部门一样,则清空
            if(tmpvo.getOrgCode().equals(tmpUser.getOrgCode())) {
                sysUserMngService.updateOrgSet(tmpUser.getUserAccount(), null);
            }
        }
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"删除成功",baseDTO,null);
    }

    public List<OrgSetMngVO> findOrgSetByUserCpyCode(String userAccount, String cpyCode) {
        return getBaseMngDao().findOrgSetByUserCpyCode(userAccount, cpyCode);
    }

    public List<OrgSetMngVO> getOrgsetByCode(Set<String> orgCodes) {
        return getBaseMngDao().getOrgsetByCode(orgCodes);
    }

}
