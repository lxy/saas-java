package com.yhy.admin.service.mng;

import com.google.common.collect.Lists;
import com.yhy.admin.dao.SysPrintMngDao;
import com.yhy.admin.dto.SysPrintDTO;
import com.yhy.admin.service.DataDictService;
import com.yhy.admin.service.SysPrintMainService;
import com.yhy.admin.vo.DataDictVO;
import com.yhy.admin.vo.mng.SysPrintMngVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseComplexMngService;
import com.yhy.common.utils.FileUtils;
import com.yhy.common.utils.JsonUtils;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysAttachment;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-31 下午5:17 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysPrintMngService extends BaseComplexMngService<SysPrintDTO, SysPrintMngVO> {

    @Autowired
    private SysPrintMngDao sysPrintMngDao;
    @Autowired
    private FtpManageService ftpManageService;
    @Autowired
    private SysPrintMainService printMainService;
    @Autowired
    private DataDictService dataDictService;

    @Override
    public SysPrintMainService getBaseMainService() {
        return printMainService;
    }

    @Override
    protected SysPrintMngDao getBaseMngDao() {
        return sysPrintMngDao;
    }

    @Override
    public String getBusModule() {
        return BusModuleType.MD_SYS_PRINT_MNG.getVal();
    }

    /*
    * 获取打印导出模板定义列表
    *  @param reportCode
    *  @param sysownerCpy
    *  @param lanCode
    */
    public List<SysPrintMngVO> findValidDataByReportCode(String reportCode, String sysOwnerCpy, String lanCode) {
        List<SysPrintMngVO> rtnList = Lists.newArrayList();
        //1.数据字典获取，即系统自定义
        DataDictVO paramDict = new DataDictVO();
        paramDict.setEnableFlag("Y");
        paramDict.setLanCode(lanCode);
        paramDict.setDictType("report_print_type");
        paramDict.setDictCode(reportCode);
        List<DataDictVO> dataDictVOS = dataDictService.findBy(paramDict);
        String printClass = "";
        try {
            for (DataDictVO dictVO : dataDictVOS) {
                SysPrintMngVO tmpVO = new SysPrintMngVO();
                tmpVO.setId(dictVO.getId());
                tmpVO.setSysOwnerCpy(sysOwnerCpy);
                tmpVO.setReportCode(dictVO.getDictCode());
                tmpVO.setReportName(dictVO.getDictName());
                tmpVO.setEnableFlag("Y");
                tmpVO.setAttachmentPath(dictVO.getAttribute1()); //系统对应的classpath路径

                String filePath = FileUtils.getTempFullPathByName(dictVO.getId() + "." + FileUtils.getExtensionName(tmpVO.getAttachmentPath()));
                if(!FileUtils.exist(filePath)) {
                    FileUtils.toFile(YhyUtils.getInputStream(dictVO.getAttribute1()), filePath);
                }
                tmpVO.setAttachmentPath(filePath);

                printClass = dictVO.getAttribute2();
                tmpVO.setPrintBean(printClass);
                tmpVO.setReportSort(0);
                rtnList.add(tmpVO);
            }
            //用户自定义的报表
            List<SysPrintMngVO> oldPrintMainVOList = getBaseMngDao().findValidDataByReportCode(sysOwnerCpy,reportCode);
            List<SysPrintMngVO> printMainVOList = JsonUtils.tranList(oldPrintMainVOList,SysPrintMngVO.class);
            ftpManageService.begin();
            for (SysPrintMngVO vo : printMainVOList) {
                vo.setPrintBean(printClass);
                String filePath = FileUtils.getTempFullPathByName(vo.getAttachmentId() + "." + FileUtils.getExtensionName(vo.getAttachmentPath()));
                if(!FileUtils.exist(filePath)) {
                    File outFile = new File(filePath);
                    ftpManageService.downloadAttachment(vo.getAttachmentPath(),outFile);
                }
                vo.setAttachmentPath(filePath);
            }
            rtnList.addAll(printMainVOList);
        } finally {
            ftpManageService.end();
        }
        return rtnList;
    }

    @Override
    protected void beforeSaveOfProcess(SysPrintDTO baseDTO) {
        super.beforeSaveOfProcess(baseDTO);
        SysPrintMngVO mngVO = baseDTO.getBusMainData();
        if(CollectionUtils.isEmpty(baseDTO.getAttachmentList())) {
            throw new BusinessException("附件不能为空");
        }
        SysAttachment sysAttachment = baseDTO.getAttachmentList().get(0);
        String attachmentId = YhyUtils.generateUUID();
        sysAttachment.setId(attachmentId);
        mngVO.setAttachmentId(sysAttachment.getId());
        mngVO.setAttribute1(sysAttachment.getFileName());
        mngVO.setAttachmentPath(baseDTO.getAttachmentList().get(0).getFilePath());
    }

    @Override
    protected void processOfToAdd(SysPrintDTO baseDTO) {
        super.processOfToAdd(baseDTO);
    }

    @Override
    protected void processOfToCopy(SysPrintDTO baseDTO) {
        super.processOfToCopy(baseDTO);
    }

    @Override
    protected void afterProcessCustomDataOfSave(SysPrintDTO baseDTO) {
        super.afterProcessCustomDataOfSave(baseDTO);
    }

}
