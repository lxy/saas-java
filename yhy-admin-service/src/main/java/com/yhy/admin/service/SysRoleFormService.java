package com.yhy.admin.service;

import com.yhy.admin.dao.SysRoleFormDao;
import com.yhy.admin.vo.SysRoleFormVO;
import com.yhy.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-20 上午9:28 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleFormService extends BaseService<SysRoleFormVO> {

	@Autowired
	private SysRoleFormDao sysRoleFormDao;

	@Override
	protected SysRoleFormDao getDao() {
		return sysRoleFormDao;
	}

	public List<SysRoleFormVO> findFormByRoleId(String roleId) {
		return getDao().findFormByRoleId(roleId);
	}

	public Set<String> findFormByRoleIds(Set<String> roleIds) {
		return getDao().findFormByRoleIds(roleIds);
	}


}
