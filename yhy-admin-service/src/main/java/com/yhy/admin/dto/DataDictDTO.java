package com.yhy.admin.dto;

import com.yhy.admin.vo.mng.DataDictMngVO;
import com.yhy.admin.vo.DataDictVO;
import com.yhy.common.dto.BaseMngDTO;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-26 下午3:02 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

public class DataDictDTO extends BaseMngDTO<DataDictMngVO> {


    @Override
    public DataDictVO getBusMain() {
        return new DataDictVO();
    }


}
